# Back

## Project description

This is the back for the cash manager mobile App. It take care of all the businesses oblique for

-creating users profiles
-auth for users
-scan products bar codes
-pass to wallet
-buy products

## Project Architecture

It's developed in clean architecture and microservices:
-user microservice
-payment microservice
-scanning microservice

## Start Server

## Push on your git

```
cd existing_repo
git remote add origin https://gitlab.com/cash-manager3/back.git
git branch -M main
git push -uf origin main
```

## Building your docker image

## Team


## Test and Deploy

Use the built-in continuous integration in GitLab.


***
